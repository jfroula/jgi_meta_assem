FROM continuumio/miniconda2

RUN conda install -c bioconda spades==3.13.1
RUN conda install -c bioconda seqtk==1.3
RUN conda install -c anaconda pigz==2.4
RUN apt-get update && apt-get install -y wget
RUN wget https://sourceforge.net/projects/bbmap/files/BBMap_38.57.tar.gz
RUN tar -zxvf BBMap_38.57.tar.gz
ENV PATH /bbmap:$PATH

RUN conda install -c bioconda samtools==1.9
RUN conda install -c bioconda bfc
RUN conda install -y openjdk==8.0.152
