workflow jgi_meta_assem_wf {
    File input_file  
    Int threads

    call rqcfilt {
      input: infile=input_file,
             threads=threads
    }

    call bfc {
      input: infile=rqcfilt.filtered,
             threads=threads
    }

    call split {
      input: infile=bfc.out
    }

    call spades {
      input: read1=split.read1,
             read2=split.read2
    }

    call fungalrelease {
      input: infile=spades.scaffolds
    }

    call stats {
      input: infile=fungalrelease.assy
    }

    call bbmap {
      input: fastq=rqcfilt.filtered,
             ref=fungalrelease.assy
    }
}

# Error correct and filter junk from reads
task rqcfilt {
    File infile
    Int threads

    command {
        #shifter --image=bryce911/bbtools:38.44 rqcfilter.sh -Xmx50G threads=${threads} in=${infile} \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 rqcfilter.sh -Xmx50G threads=${threads} in=${infile} \
           out=filtered.fastq.gz \
           path=. trimfragadapter=t qtrim=r trimq=10 maxns=0 maq=12 minlen=51 mlf=0.33 \
           phix=t removehuman=t removedog=t removecat=t removemouse=t khist=t removemicrobes=f sketch=f kapa=t clumpify=t
    }

    output {
        File filtered = "filtered.fastq.gz"
    }
}

task bfc {
    File infile 
    Int threads
    
    command
    {
        echo -e "\n### running bfc infile: ${infile} ###\n" 
        #shifter --image=jfroula/bfc:181  \
        shifter --image=jfroula/jgi_meta_assem:1.0.2  \
            bfc -1 -k 25 -b 32 -t ${threads} ${infile} 1> bfc.fastq 2> bfc.error 

        #shifter --image=jfroula/jgi_meta_annot:1.1.3 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            seqtk trimfq bfc.fastq 1> bfc.seqtk.fastq 2> seqtk.error
        
        #shifter --image=jfroula/jgi_meta_annot:1.1.2 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            pigz -c bfc.seqtk.fastq -p 4 -2 1> filtered.bfc.fq.gz 2> pigz.error 
    }
    output {
        File out = "filtered.bfc.fq.gz"
    }
}

# split paired end reads for spades (and generate some read stats)
task split {
    File infile 
    
    command
    {
        #shifter --image=bryce911/bbtools:38.44 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            readlength.sh in=${infile} 1>| readlen.txt;

        #shifter --image=bryce911/bbtools:38.44 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            reformat.sh overwrite=true in=${infile} out=read1.fasta out2=read2.fasta
    }
    output {
        File readlen = "readlen.txt"
        File read1 = "read1.fasta"
        File read2 = "read2.fasta"
    }
}

# run MetaSpades
task spades {
    File read1
    File read2
    
    command
    {
        #shifter --image=bryce911/spades:3.13.0  \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            spades.py -m 2000 -o spades3 --only-assembler -k 33,55,77,99,127 --meta -t 32 -1 ${read1} -2 ${read2}
    }
    output {
        File scaffolds = "spades3/scaffolds.fasta"
    }
    runtime {
        cpu: 32
        time: "24:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "annotation"
    }
}

# fungalrelease step will improve conteguity of assembly
task fungalrelease {
    File infile
    
    command
    {
        #shifter --image=bryce911/bbtools:38.44 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            fungalrelease.sh -Xmx10g in=${infile} out=assembly.scaffolds.fasta outc=assembly.contigs.fasta agp=assembly.agp legend=assembly.scaffolds.legend mincontig=200 minscaf=200 sortscaffolds=t sortcontigs=t overwrite=t
    }
    output {
        File assy = "assembly.scaffolds.fasta"
    }
}

# generate some assembly stats for the report
task stats {
    File infile
    
    command
    {
        #shifter --image=bryce911/bbtools:38.44 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            stats.sh format=6 in=${infile} 1> bbstats.tsv 2> bbstats.tsv.e 

        #shifter --image=bryce911/bbtools:38.44 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            stats.sh in=${infile} 1> bbstats.txt 2> bbstats.txt.e
    }
    output {
        File statstsv = "bbstats.tsv"
        File statstxt = "bbstats.txt"
    }
}

# Map the filtered (but not bfc corrected) reads back to scaffold
task bbmap {
    File fastq
    File ref
    
    command
    {
        #shifter --image==bryce911/bbtools:38.44 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            bbmap.sh nodisk=true interleaved=true ambiguous=random in=${fastq} ref=${ref} out=paired.mapped.bam covstats=paired.mapped.cov 

        #shifter --image=jfroula/jgi_meta_annot:1.1.2 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            samtools sort -@ 3 paired.mapped.bam -o paired.mapped_sorted.bam

        #shifter --image=jfroula/jgi_meta_annot:1.1.2 \
        shifter --image=jfroula/jgi_meta_assem:1.0.2 \
            samtools index paired.mapped_sorted.bam
    }
    output {
        File bam = "paired.mapped.bam"
        File covstats = "paired.mapped.cov"
    }
}


